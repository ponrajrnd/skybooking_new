<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'skybooking');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aQl}B56lw__B`Wjldsg,99Nj$/#hS(B+Or}qlWEsUu&8ks5!6ZW9KJiLtv=?,{Dm');
define('SECURE_AUTH_KEY',  'krWyl.unw*U3po0O>`}tR|G*Ti~/Pfu6fa_~Y^]SgVu-(66);_!tt!,GI$PL^2~z');
define('LOGGED_IN_KEY',    'i]>S=)P_T5KLz]zM;a2VEZL69 Y,^:,RI]k`sl%i<aXkDC4j${-vL&tL|!@zX~U`');
define('NONCE_KEY',        'Ule`aZlhU0JNO%1&E</RJ|A*F%bi|L-pxs&Z|C$rpX7IX0==pEe*A9:R_>nJm>3Y');
define('AUTH_SALT',        '>EgfMrSI?zzR2tJWr[8K(C~%v^Z#u7KkV1T@Q1]UJ>FPmy_.1(7-R5)w[)?wJ1,9');
define('SECURE_AUTH_SALT', 'Q,rZt_hqdg/HqGqd1U3Ob#mv!XguH%+<cLp1&_}n-CK,-H@W^ChjO1>bg@)yz-p#');
define('LOGGED_IN_SALT',   '9PQVNMs?GfoW:pJkoKr6!}q)0&6}yWFAqOYh}uh#=|v8RvK^LDtF[u:J!i2&CvJ3');
define('NONCE_SALT',       '%]FCC*[-)]/[{;;(+N~ZqS-|j^uMM3W!s2:e;B_,I91@QfS+v]kIyi:)3$OY~cDU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
