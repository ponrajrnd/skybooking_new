<?php
/**
 * Created by wpbooking.
 * Developer: nasanji
 * Date: 6/14/2017
 * Version: 1.0
 */
extract($data);
if(!isset($field_size)) $field_size='lg';
$title = st_the_language('cabin_class');
?>
<div class="form-group form-group-select-plus flight-input form-group-<?php echo esc_attr($field_size)?>">
    <label><?php echo $title;?></label>
    <select id="cabin_class" class="form-control" name="cabin_class">
        <option value="Economy">Economy</option>
        <option value="Premium Economy">Premium Economy</option>
        <option value="Promotion">Promotion</option>
    </select>
</div>
