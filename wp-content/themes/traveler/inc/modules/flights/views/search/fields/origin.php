<?php
/**
 * Created by wpbooking.
 * Developer: nasanji
 * Date: 6/14/2017
 * Version: 1.0
 */

wp_enqueue_style( 'st-flight-select-css' );
wp_enqueue_script( 'st-flight-select-js' );



$default=array(
    'title'=>'',
    'placeholder'=>'',
    'is_required'=>'on',
);
if(isset($data)){
    extract(wp_parse_args($data,$default));
}else{
    extract($default);
}
if($is_required == 'on'){
    $is_required = 'required';
}

if(!isset($field_size)) $field_size='lg';

$origin = STInput::get('origin', '');
$origin_name = STInput::get('origin_name', '');
$list_airport = get_terms('st_airport', array(
    'hide_empty' => false
));
$locations = TravelHelper::getListFlightLocation( 'st_airport' );
?>
<div class="form-group form-group-<?php echo esc_attr($field_size)?> form-group-icon-left flight-input">
    <label for="field-hotel-location"><?php echo esc_attr($title); ?></label>
    <i class="fa fa-plane input-icon"></i>
    <div class="st-select-wrapper">
        <input id="field-hotel-location" autocomplete="off" type="text" name="origin_name" value="<?php echo esc_attr($origin_name); ?>" class="form-control st-flight-location-name <?php echo esc_attr($is_required); ?>" placeholder="<?php if($placeholder) echo $placeholder; ?>">
        <select id="field-hotel-location" name="origin" class="st-flight-location-id st-hidden" placeholder="<?php if($placeholder) echo $placeholder; ?>" tabindex="-1">
            <?php
                if(is_array($locations) && count($locations)):
                    foreach($locations as $key => $value):
            ?>
            <option value="<?php echo $value->iata_id.'--'.$value->airport_id; ?>"><?php echo $value->fullname.' - '.$value->iata_id; ?></option>
            <?php endforeach; endif; ?>
        </select>
        <div class="option-wrapper"></div>
    </div>
</div>
<!--<script>
$(".st-flight-location-name").keyup(function(e){
        e.preventDefault();
        var search_val=$("input[name=origin_name]").val(); 
        $.ajax({
            type:"GET",
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            data: {
                action:'airport_search',
                name:search_val
            },
			dataType: "json",
            success:function(jsonStr){
				var obj = JSON.stringify(jsonStr);
				alert(obj);
                //$('#search_result').html(data);
            }
        }); 
});
</script>-->