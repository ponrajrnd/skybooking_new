<?php
/**
 * Created by wpbooking.
 * Developer: nasanji
 * Date: 6/16/2017
 * Version: 1.0
 */
global $wp_query, $st_flight_search_query, $st_flight_search_return_query;

wp_enqueue_script('magnific.js');

if(!empty($st_flight_search_query)){
    $query = $st_flight_search_query;
}else{
    $query = $wp_query;
}

if(!empty($st_flight_search_return_query)){
    $query2 = $st_flight_search_return_query;
}else{
    $query2 = $wp_query;
}

$has_post_depart = false;
$has_post_return = false;

$f_type = STInput::get('flight_type', false);
if((int)$query2->found_posts > 0 && $f_type == 'return'){
    $has_post_return = true;
}

//var_dump($query->request);

if((int)$query->found_posts > 0){
    if($f_type == 'return'){
        if($has_post_return)
            $has_post_depart = true;
    }else{
        $has_post_depart = true;
    }
}

if(!$has_post_depart){
    $has_post_return = false;
}

$return_date = STInput::get('end');
$depart_date = STInput::get('start');
$origin = STInput::get('origin');
$destination = STInput::get('destination');
$passenger = STInput::get('passenger');
$business = STInput::get('business', false);
$from_id = $to_id = '';
$origin_iata = $destination_iata = '';
if(!empty(explode('--', $origin)[1])){
    $origin_iata = explode('--', $origin)[0];
    $from_id = explode('--', $origin)[1];
}
if(!empty(explode('--', $destination)[1])){
    $destination_iata = explode('--', $destination)[0];
    $to_id = explode('--', $destination)[1];
}
$location_from = get_tax_meta($from_id, 'location_id');
$location_to = get_tax_meta($to_id, 'location_id');
$on_way = true;
if($f_type == 'return'){
    $on_way = false;
}?>

<!--<div class="col-md-12" id="fare_title"><span class="fare_select">Select your fare</span></div>-->
<div class="col-md-12" id="fare_title"><h3 class="fare_select">Select your fare</h3></div>
<!--<div class="col-md-12" style="margin-top:20px">
<div class="col-sm-3 economy_price"><span>Economy</span></div>
<div class="col-sm-3 premium_price"><span>Premium</span></div>
<div class="col-sm-3 promotion_price"><span>Promotion</span></div>
</div>-->
<div class="st-flight-booking-result">
<div class="st-booking-list">
<?php if($f_type == 'one_way'){
    $has_post = true;
    //if(!empty($location_from) && !empty($location_to)){
?>
<div class="departure-title">
	<h4 class="title"><?php echo get_the_title($location_from).' ('.$origin_iata.')' ?>
	<?php echo esc_html__('to', ST_TEXTDOMAIN); ?>
		<?php echo get_the_title($location_to).' ('.$destination_iata.')'; ?>&nbsp
		<i class="fa fa-plane" aria-hidden="true"></i>
	</h4>
	
</div>
<?php //} ?>
<div class="col-md-12" style="height: 50px;">

<div class="col-sm-6 col-lg-8" style="padding-left: 150px;"><span class="depart_flight">Select departing flight</span></div><div class="col-sm-6 col-lg-4"><span class="depart-oneway">Fri 05 Jan</span></div>

</div>
<ul class="booking-list  depart st-booking-list-flight" data-flight_type="<?php echo ($on_way?'on_way':'return'); ?>">
    <?php
    while($query->have_posts()){
        $query->the_post();
        $start = STInput::get('start', '');
        $data_time = st_flight_get_duration(get_the_ID());
        $flight_type = get_post_meta(get_the_ID(),'flight_type', true);
        $stop_info = st_flight_get_info_stop(get_the_ID());
    ?>
    <li>
        <div class="col-md-10 booking-item-container" id="flight_list">
            <div class="booking-item flight-item-<?php echo esc_attr(get_the_ID())?>" style="margin-left: 150px;margin-right: 80px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="booking-item-airline-logo">
                            <?php
                            $airline = get_post_meta(get_the_ID(), 'airline', true);
                            if(!empty($airline)) {
                                $air_object = get_term_by('id', $airline, 'st_airline');
                                if (!empty($air_object->name)) {
                                    $air_logo = get_tax_meta($airline, 'airline_logo');
                                    echo wp_get_attachment_image($air_logo, array(0, 50));
                                    ?>
                                <?php }
                            } ?>
                        </div>
                    </div>
                    <?php
                    $type = array(
                        'direct' => esc_html__('non-stop', ST_TEXTDOMAIN),
                        'one_stop' => esc_html__('one stop', ST_TEXTDOMAIN),
                        'two_stops' => esc_html__('two stops', ST_TEXTDOMAIN),
                    );
                    ?>
                    <div class="col-md-12">
                        <div class="booking-item-flight-details">
                            <div class="booking-item-departure">
                                <h5><?php echo !empty($data_time['depart_time'])?strtoupper($data_time['depart_time']):''; ?></h5>
                                <p class="booking-item-date"><?php echo !empty($data_time['depart_date'])?strtoupper($data_time['depart_date']):''; ?></p>
                            </div>
                            <div class="booking-item-arrival">
                                <h5><?php echo !empty($data_time['arrive_time'])?strtoupper($data_time['arrive_time']):''; ?></h5>
                                <p class="booking-item-date"><?php echo !empty($data_time['arrive_date'])?strtoupper($data_time['arrive_date']):''; ?></p>
                            </div>
                            <div class="flight-layovers">
                                <div class="header duration"><?php echo !empty($data_time['total_time'])?strtoupper($data_time['total_time']):''; ?></div>

                                <div class="flight-line">
                                    <div class="origin">
                                        <div class="origin-iata">
                                            <?php
                                            echo esc_attr($origin_iata);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="destination">
                                        <div class="destination-iata">
                                            <?php
                                            echo esc_attr($destination_iata);
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    if($flight_type == 'one_stop'){
                                        ?>
                                        <div class="stop">
                                            <div class="iata-stop">
                                                <?php
                                                $stop1 = get_post_meta(get_the_ID(), 'airport_stop', true);
                                                $stop1_iata = get_tax_meta($stop1 , 'iata_airport');
                                                echo esc_attr($stop1_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if($flight_type == 'two_stops'){
                                        ?>
                                        <div class="stop1">
                                            <div class="iata-stop1">
                                                <?php
                                                $stop1 = get_post_meta(get_the_ID(), 'airport_stop_1', true);
                                                $stop1_iata = get_tax_meta($stop1 , 'iata_airport');
                                                echo esc_attr($stop1_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <div class="stop2">
                                            <div class="iata-stop2">
                                                <?php
                                                $stop2 = get_post_meta(get_the_ID(), 'airport_stop_2', true);
                                                $stop2_iata = get_tax_meta($stop2 , 'iata_airport');
                                                echo esc_attr($stop2_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                if($flight_type == 'direct'){
                                    echo '<div class="footer">'.esc_html__('Direct Flight', ST_TEXTDOMAIN).'</div>';
                                 } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $price_eco_flight = ST_Flights_Controller::inst()->get_price_flight(get_the_ID(), strtotime(TravelHelper::convertDateFormat($start)), false);
                    $price_buss_flight = ST_Flights_Controller::inst()->get_price_flight(get_the_ID(), strtotime(TravelHelper::convertDateFormat($start)), true);

                    $enable_tax = get_post_meta(get_the_ID(),'enable_tax',true);
                    $vat_amount = get_post_meta(get_the_ID(),'vat_amount',true);
                    ?>
                    <!--<div class="col-md-12 text-center st-flight-price">
                        <?php if($price_eco_flight > 0){ ?>
                        <div class="eco-price st-cal-flight-depart">
                            <span class="booking-item-price"><?php echo TravelHelper::format_money($price_eco_flight); ?></span><span>/<?php echo esc_html__('person', ST_TEXTDOMAIN); ?></span>
                            <p class="booking-item-flight-class"><?php echo esc_html__('Class', ST_TEXTDOMAIN)?>: <?php echo esc_html__('Economy', ST_TEXTDOMAIN)?></p>
                            <input class="st-choose-flight-depart i-radio" data-tax="<?php echo esc_attr($enable_tax); ?>" data-tax_amount="<?php echo esc_attr($vat_amount); ?>" data-flight_type="depart" type="radio" data-post_id="<?php echo get_the_ID(); ?>" data-price="<?php echo esc_attr($price_eco_flight); ?>" data-business="0" name="flight1" value="<?php echo esc_attr(get_the_ID()); ?>">
                        </div>
                        <?php }
                        if($price_buss_flight > 0){
                        ?>
                        <div class="bus-price st-cal-flight-depart">
                            <span class="booking-item-price"><?php echo TravelHelper::format_money($price_buss_flight); ?></span><span>/<?php echo esc_html__('person', ST_TEXTDOMAIN); ?></span>
                            <p class="booking-item-flight-class"><?php echo esc_html__('Class', ST_TEXTDOMAIN)?>: <?php echo esc_html__('Business', ST_TEXTDOMAIN)?></p>
                            <input class="st-choose-flight-depart i-radio" data-tax="<?php echo esc_attr($enable_tax); ?>" data-tax_amount="<?php echo esc_attr($vat_amount); ?>" data-flight_type="depart" type="radio" data-post_id="<?php echo get_the_ID(); ?>" data-price="<?php echo esc_attr($price_buss_flight); ?>" data-business="1" name="flight1" value="<?php echo esc_attr(get_the_ID()); ?>">
                        </div>
                        <?php } ?>
                    </div>-->
                </div>
            </div>
<!--        flight detail       -->
            <div class="booking-item-details" id="flight-information" style="margin-left: 150px;margin-right: 80px;">
                <div class="row">
                    <div class="col-md-12">
                        <h5><?php st_the_language('flight_details'); ?></h5>
                        <?php
						 $airline = get_post_meta(get_the_ID(),'airline', true);
						 
                        if($flight_type == 'direct'){
                            //echo '<h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.st_flight_get_airport_meta('iata_from').') '.esc_html__('to', ST_TEXTDOMAIN).' '.get_the_title(st_flight_get_airport_meta('location_to')).' ('.st_flight_get_airport_meta('iata_to').')</h5>';
                            echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div><div class="col-sm-8"><h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.st_flight_get_airport_meta('iata_from').')  -  '.get_the_title(st_flight_get_airport_meta('location_to')).' ('.st_flight_get_airport_meta('iata_to').')</h5></div>'.'<div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
                           
                            ?>
                            <ul class="list">
                                <?php
								$air_logo = get_tax_meta($airline, 'airline_logo');
								$air_line_name = $stop_info['airline_name'];
                                echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
								echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($depart_date)).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrive_time'].'</span></div>';
                                echo '<div class="col-sm-10" id="flight_airlines"><span>'.$air_line_name.' '.$query->post->post_title.'</span></div>';
                                echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
                                ?>
                            </ul>
                            <?php
                        }
                        if($flight_type == 'one_stop'){
                            echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div>
							<div class="col-sm-8">
							<h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.$stop_info['origin_iata'].')  -  '.get_the_title($stop_info['airport_stop_location']).' ('.$stop_info['airport_stop_iata'].')</h5></div><div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
							echo '<ul class="list">';
							$air_logo = get_tax_meta($airline, 'airline_logo');
							$air_line_name = $stop_info['airline_name'];
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($depart_date)).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrival_stop_time'].'</span></div>';
                            echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
							echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';

                            echo '<div class="col-sm-2"><span id="flight_depart">Stop 1</span></div>
							<div class="col-sm-8">
							<h5 class="list-title">'.get_the_title($stop_info['airport_stop_location']).' ('.$stop_info['airport_stop_iata'].') -  '.get_the_title($stop_info['destination_location']).' ('.$stop_info['destination_iata'].')</h5></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time'].' - '.$data_time['arrive_time'].'</span></div>';
   							echo '<div class="col-sm-5" id="flight_airlines"><span>'.$stop_info['airline_stop_name'].' '.$query->post->post_title.'</span></div>';
                            echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
							echo '</ul>';
                        }
                        if($flight_type == 'two_stops'){
							echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.$origin_name[0].' ('.$stop_info['origin_iata'].')  -   '.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].')</h5></div><div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
                            echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date_1'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrival_stop_time_1'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
                            echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';
                           

                            //echo ' <div class="col-sm-12"><h5>'.st_the_language('stopover').' '.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].') '.$data_time['st_stopover_time_1'].'</h5></div>';
							
							echo '<div class="col-sm-2"><span id="flight_depart">Stop 1</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].')  -  '.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].')</h5></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
                            echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date_2'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time_1'].' - '.$data_time['arrival_stop_time_2'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
                            echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';
                            //echo '</ul><div class="col-sm-12"><span class="flight_change">Change planes in '.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].')</span></div>&nbsp&nbsp';

                            //echo '<div class="col-sm-12"><h5>'.st_the_language('stopover').' '.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].') '.$data_time['st_stopover_time_2'].'</h5></div>';
							
							echo '<div class="col-sm-2"><span id="flight_depart">Stop 2</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].')  -  '.get_the_title($stop_info['destination_location']).' ('.$stop_info['destination_iata'].')</h5></div>';
                            echo '<ul class="list">';
                            echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['arrive_date'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time_2'].' - '.$data_time['arrive_time'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
							echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
							echo '</ul>';
                        }
                        ?>


                        <?php
                        if (!empty($data_time['total_time'])) {
                            ?>
                            <!--<p><?php st_the_language('tour_trip_time') ?>
                                : <?php echo esc_attr($data_time['total_time']); ?></p>-->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </li>
        <?php } ?>
</ul>
    <div class="row">
        <?php
            TravelHelper::paging_flight($query);
        ?>

    </div>
<?php }else{ ?>
<div class="departure-title">
	<h4 class="title"><?php echo get_the_title($location_from).' ('.$origin_iata.')' ?>
	<?php echo esc_html__('to', ST_TEXTDOMAIN); ?>
		<?php echo get_the_title($location_to).' ('.$destination_iata.')'; ?>&nbsp
		<i class="fa fa-plane" aria-hidden="true"></i>
	</h4>
	
</div>
<div class="col-md-12" style="height: 50px;">
<div class="col-sm-3" style="padding-left: 3px;"><span class="depart_flight">Select departing flight</span></div><div class="col-sm-3"><span class="depart-date">Fri 05 Jan</span></div>
<div class="col-sm-3"><span class="return_flight">Select returning flight</span></div><div class="col-sm-3"><span class="return-date">Fri 12 Jan</span></div>
</div>
<ul class="booking-list  depart st-booking-list-flight" data-flight_type="<?php echo ($on_way?'on_way':'return'); ?>">
    <?php
    while($query->have_posts()){
        $query->the_post();
        $start = STInput::get('start', '');
        $data_time = st_flight_get_duration(get_the_ID());
        $flight_type = get_post_meta(get_the_ID(),'flight_type', true);
        $stop_info = st_flight_get_info_stop(get_the_ID());
    ?>
    <li>
        <div class="col-md-6 booking-item-container" id="flight_list">
            <div class="booking-item flight-item-<?php echo esc_attr(get_the_ID())?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="booking-item-airline-logo">
                            <?php
                            $airline = get_post_meta(get_the_ID(), 'airline', true);
                            if(!empty($airline)) {
                                $air_object = get_term_by('id', $airline, 'st_airline');
                                if (!empty($air_object->name)) {
                                    $air_logo = get_tax_meta($airline, 'airline_logo');
                                    echo wp_get_attachment_image($air_logo, array(0, 50));
                                    ?>
                                <?php }
                            } ?>
                        </div>
                    </div>
                    <?php
                    $type = array(
                        'direct' => esc_html__('non-stop', ST_TEXTDOMAIN),
                        'one_stop' => esc_html__('one stop', ST_TEXTDOMAIN),
                        'two_stops' => esc_html__('two stops', ST_TEXTDOMAIN),
                    );
                    ?>
                    <div class="col-md-12">
                        <div class="booking-item-flight-details">
                            <div class="booking-item-departure">
                                <h5><?php echo !empty($data_time['depart_time'])?strtoupper($data_time['depart_time']):''; ?></h5>
                                <p class="booking-item-date"><?php echo !empty($data_time['depart_date'])?strtoupper($data_time['depart_date']):''; ?></p>
                            </div>
                            <div class="booking-item-arrival">
                                <h5><?php echo !empty($data_time['arrive_time'])?strtoupper($data_time['arrive_time']):''; ?></h5>
                                <p class="booking-item-date"><?php echo !empty($data_time['arrive_date'])?strtoupper($data_time['arrive_date']):''; ?></p>
                            </div>
                            <div class="flight-layovers">
                                <div class="header duration"><?php echo !empty($data_time['total_time'])?strtoupper($data_time['total_time']):''; ?></div>

                                <div class="flight-line">
                                    <div class="origin">
                                        <div class="origin-iata">
                                            <?php
                                            echo esc_attr($origin_iata);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="destination">
                                        <div class="destination-iata">
                                            <?php
                                            echo esc_attr($destination_iata);
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    if($flight_type == 'one_stop'){
                                        ?>
                                        <div class="stop">
                                            <div class="iata-stop">
                                                <?php
                                                $stop1 = get_post_meta(get_the_ID(), 'airport_stop', true);
                                                $stop1_iata = get_tax_meta($stop1 , 'iata_airport');
                                                echo esc_attr($stop1_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if($flight_type == 'two_stops'){
                                        ?>
                                        <div class="stop1">
                                            <div class="iata-stop1">
                                                <?php
                                                $stop1 = get_post_meta(get_the_ID(), 'airport_stop_1', true);
                                                $stop1_iata = get_tax_meta($stop1 , 'iata_airport');
                                                echo esc_attr($stop1_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <div class="stop2">
                                            <div class="iata-stop2">
                                                <?php
                                                $stop2 = get_post_meta(get_the_ID(), 'airport_stop_2', true);
                                                $stop2_iata = get_tax_meta($stop2 , 'iata_airport');
                                                echo esc_attr($stop2_iata);
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                if($flight_type == 'direct'){
                                    echo '<div class="footer">'.esc_html__('Direct Flight', ST_TEXTDOMAIN).'</div>';
                                 } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $price_eco_flight = ST_Flights_Controller::inst()->get_price_flight(get_the_ID(), strtotime(TravelHelper::convertDateFormat($start)), false);
                    $price_buss_flight = ST_Flights_Controller::inst()->get_price_flight(get_the_ID(), strtotime(TravelHelper::convertDateFormat($start)), true);

                    $enable_tax = get_post_meta(get_the_ID(),'enable_tax',true);
                    $vat_amount = get_post_meta(get_the_ID(),'vat_amount',true);
                    ?>
                    <!--<div class="col-md-12 text-center st-flight-price">
                        <?php if($price_eco_flight > 0){ ?>
                        <div class="eco-price st-cal-flight-depart">
                            <span class="booking-item-price"><?php echo TravelHelper::format_money($price_eco_flight); ?></span><span>/<?php echo esc_html__('person', ST_TEXTDOMAIN); ?></span>
                            <p class="booking-item-flight-class"><?php echo esc_html__('Class', ST_TEXTDOMAIN)?>: <?php echo esc_html__('Economy', ST_TEXTDOMAIN)?></p>
                            <input class="st-choose-flight-depart i-radio" data-tax="<?php echo esc_attr($enable_tax); ?>" data-tax_amount="<?php echo esc_attr($vat_amount); ?>" data-flight_type="depart" type="radio" data-post_id="<?php echo get_the_ID(); ?>" data-price="<?php echo esc_attr($price_eco_flight); ?>" data-business="0" name="flight1" value="<?php echo esc_attr(get_the_ID()); ?>">
                        </div>
                        <?php }
                        if($price_buss_flight > 0){
                        ?>
                        <div class="bus-price st-cal-flight-depart">
                            <span class="booking-item-price"><?php echo TravelHelper::format_money($price_buss_flight); ?></span><span>/<?php echo esc_html__('person', ST_TEXTDOMAIN); ?></span>
                            <p class="booking-item-flight-class"><?php echo esc_html__('Class', ST_TEXTDOMAIN)?>: <?php echo esc_html__('Business', ST_TEXTDOMAIN)?></p>
                            <input class="st-choose-flight-depart i-radio" data-tax="<?php echo esc_attr($enable_tax); ?>" data-tax_amount="<?php echo esc_attr($vat_amount); ?>" data-flight_type="depart" type="radio" data-post_id="<?php echo get_the_ID(); ?>" data-price="<?php echo esc_attr($price_buss_flight); ?>" data-business="1" name="flight1" value="<?php echo esc_attr(get_the_ID()); ?>">
                        </div>
                        <?php } ?>
                    </div>-->
                </div>
            </div>
<!--        flight detail       -->
            <div class="booking-item-details" id="flight-information">
                <div class="row">
                    <div class="col-md-12">
                        <h5><?php st_the_language('flight_details'); ?></h5>
                        <?php
						 $airline = get_post_meta(get_the_ID(),'airline', true);
						 
                        if($flight_type == 'direct'){
                            //echo '<h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.st_flight_get_airport_meta('iata_from').') '.esc_html__('to', ST_TEXTDOMAIN).' '.get_the_title(st_flight_get_airport_meta('location_to')).' ('.st_flight_get_airport_meta('iata_to').')</h5>';
                            echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div><div class="col-sm-8"><h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.st_flight_get_airport_meta('iata_from').')  -  '.get_the_title(st_flight_get_airport_meta('location_to')).' ('.st_flight_get_airport_meta('iata_to').')</h5></div>'.'<div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
                           
                            ?>
                            <ul class="list">
                                <?php
								$air_logo = get_tax_meta($airline, 'airline_logo');
								$air_line_name = $stop_info['airline_name'];
                                echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
								echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($depart_date)).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrive_time'].'</span></div>';
                                echo '<div class="col-sm-10" id="flight_airlines"><span>'.$air_line_name.' '.$query->post->post_title.'</span></div>';
                                echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
                                ?>
                            </ul>
                            <?php
                        }
                        if($flight_type == 'one_stop'){
                            echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div>
							<div class="col-sm-8">
							<h5 class="list-title">'.get_the_title(st_flight_get_airport_meta('location_from')).' ('.$stop_info['origin_iata'].')  -  '.get_the_title($stop_info['airport_stop_location']).' ('.$stop_info['airport_stop_iata'].')</h5></div><div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
							echo '<ul class="list">';
							$air_logo = get_tax_meta($airline, 'airline_logo');
							$air_line_name = $stop_info['airline_name'];
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($depart_date)).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrival_stop_time'].'</span></div>';
                            echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
							echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';

                            echo '<div class="col-sm-2"><span id="flight_depart">Stop 1</span></div>
							<div class="col-sm-8">
							<h5 class="list-title">'.get_the_title($stop_info['airport_stop_location']).' ('.$stop_info['airport_stop_iata'].') -  '.get_the_title($stop_info['destination_location']).' ('.$stop_info['destination_iata'].')</h5></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time'].' - '.$data_time['arrive_time'].'</span></div>';
   							echo '<div class="col-sm-5" id="flight_airlines"><span>'.$stop_info['airline_stop_name'].' '.$query->post->post_title.'</span></div>';
                            echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
							echo '</ul>';
                        }
                        if($flight_type == 'two_stops'){
							echo '<div class="col-sm-2"><span id="flight_depart">Departure</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.$origin_name[0].' ('.$stop_info['origin_iata'].')  -   '.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].')</h5></div><div class="col-sm-2"><span class="flight_total_time">'.$data_time['total_time'].'</span></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
                            echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date_1'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['depart_time'].' - '.$data_time['arrival_stop_time_1'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
                            echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';
                           

                            //echo ' <div class="col-sm-12"><h5>'.st_the_language('stopover').' '.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].') '.$data_time['st_stopover_time_1'].'</h5></div>';
							
							echo '<div class="col-sm-2"><span id="flight_depart">Stop 1</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.get_the_title($stop_info['airport_stop_1_location']).' ('.$stop_info['airport_stop_1_iata'].')  -  '.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].')</h5></div>';
                            echo '<ul class="list">';
							echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
                            echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['departure_stop_date_2'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time_1'].' - '.$data_time['arrival_stop_time_2'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
                            echo '</ul><div class="col-sm-12"></div>&nbsp&nbsp';
                            
                            //echo '<div class="col-sm-12"><h5>'.st_the_language('stopover').' '.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].') '.$data_time['st_stopover_time_2'].'</h5></div>';
							
							echo '<div class="col-sm-2"><span id="flight_depart">Stop 2</span></div>
							<div class="col-sm-8">';
                            echo '<h5 class="list-title">'.get_the_title($stop_info['airport_stop_2_location']).' ('.$stop_info['airport_stop_2_iata'].')  -  '.get_the_title($stop_info['destination_location']).' ('.$stop_info['destination_iata'].')</h5></div>';
                            echo '<ul class="list">';
                            echo '<div class="col-sm-2" id="flight_image">'.wp_get_attachment_image($air_logo, array(0, 30)).'</div>';
							echo '<div class="col-sm-10" id="flight_date">'.date("D, M d", strtotime($data_time['arrive_date'])).'&nbsp&nbsp&nbsp&nbsp'.'<span class="flight_duration">'.$data_time['departure_stop_time_2'].' - '.$data_time['arrive_time'].'</span></div>';
   							echo '<div class="col-sm-10" id="flight_airlines"><span>'.$stop_info['airline_name'].' '.$query->post->post_title.'</span></div>';
							echo '<div class="col-sm-5" id="flight_baggage"><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp&nbsp<span>Checked-In:25 kgs for adult</span></div>';
							echo '</ul>';
                        }
                        ?>


                        <?php
                        if (!empty($data_time['total_time'])) {
                            ?>
                            <!--<p><?php st_the_language('tour_trip_time') ?>
                                : <?php echo esc_attr($data_time['total_time']); ?></p>-->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </li>
        <?php } ?>
</ul>
<?php }?>

</div>
</div>
<?php
    echo st_flight_load_view('flights/search-flights');
?>