<?php

/**
* @package  Wordpress 
* @subpackage shinetheme
* @since 1.1.3
*/

/**@update 1.1.5*/
class st_location_summary_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'st_location_summary_widget', 
			__('ST Location Summary', ST_TEXTDOMAIN), 
			array( 'description' => __( 'Show summary by post type', ST_TEXTDOMAIN ), ) 
		);
		add_action('admin_enqueue_scripts',array($this,'add_scripts'));

	    
	}
	public function add_scripts(){
		$screen=get_current_screen();
    if($screen->base=='widgets'){
    	wp_enqueue_style('jquery-ui',get_template_directory_uri().'/css/admin/jquery-ui.min.css');
        wp_enqueue_script('location_widget',get_template_directory_uri().'/js/admin/widgets/location_widget.js',array('jquery','jquery-ui-sortable'),null,true);            
    }
	}

	public function widget( $args, $instance ) {	
		$instance=wp_parse_args($instance,array(
				'post_type'=>'',		
			) 
		);
		$title                = apply_filters( 'widget_title', $instance['title'] );
		$title 								= apply_filters( 'widget_title', empty( $title ) ? '' : $title, $instance, $this->id_base );
		$instance['title']    = $title;
		extract($instance);		
		echo $args['before_widget'];
		echo apply_filters('the_content',st()->load_template('location/widget/summary' , NULL,array('instance'=>$instance)));
		echo $args['after_widget'];
	}
		
	public function form( $instance ) { 		
		$instance=wp_parse_args($instance,array(
				'title'	=> '',		
				'post_type'=>'',	
			) );
		extract($instance);
		?>
		<div class='location_widget_item'>
			<p>
				<label> <?php _e( 'Title', ST_TEXTDOMAIN ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label><?php _e( 'Select post type'  , ST_TEXTDOMAIN ); ?></label> 
				<select name="<?php echo esc_attr($this->get_field_name( 'post_type' )); ?>" >
					<?php $a = new STCars() ;if ($a->is_available()) { ?> <option <?php if ($post_type =="st_cars") echo esc_attr("selected") ; ?>  value='st_cars'>Car</option> <?php }; ?>
					<?php $a = new STHotel() ;if ($a->is_available()) { ?> <option <?php if ($post_type =="st_hotel") echo esc_attr("selected") ; ?>  value='st_hotel'>Hotel</option> <?php }; ?>
					<?php $a = new STRental() ;if ($a->is_available()) { ?> <option <?php if ($post_type =="st_rental") echo esc_attr("selected") ; ?>  value='st_rental'>Rental</option> <?php }; ?>
					<?php $a = new STTour() ;if ($a->is_available()) { ?> <option <?php if ($post_type =="st_tours") echo esc_attr("selected") ; ?>  value='st_tours'>Tour</option> <?php }; ?>
					<?php $a = STActivity::inst() ;if ($a->is_available()) { ?> <option <?php if ($post_type =="st_activity") echo esc_attr("selected") ; ?>  value='st_activity'>Activity</option> <?php }; ?>
				</select>
			</p>
		</div>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';	
		$instance['post_type'] = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : '';
		return $instance;
	}
} // Class st_location_summary_widget ends here

// Register and load the widget
function st_location_summary_widget() {
	register_widget( 'st_location_summary_widget' );
}
add_action( 'widgets_init', 'st_location_summary_widget' );