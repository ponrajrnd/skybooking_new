<div class="container bg-partner-new <?php echo esc_html($sc) ?>">
	<div class="row row_content_partner">
    <div class="col-md-3 user-left-menu ">
      <div class="page-sidebar navbar-collapse st-page-sidebar-new">
      	<ul class="page-sidebar-menu st_menu_new">
          <li class="heading text-center user-profile-sidebar">
            <div class="user-profile-avatar text-center">
              <?php echo st_get_profile_avatar($current_user->ID, 300); ?>
              <h5><?php echo esc_html($current_user->display_name) ?></h5>

              <p><?php echo st_get_language('user_member_since') . mysql2date(' M Y', $current_user->data->user_registered); ?></p>
            </div>
          </li>
          <li class="item <?php if($sc == 'dashboard') echo 'active' ?> ">
            <a href="<?php echo add_query_arg( 'id_user', $url_id_user ,TravelHelper::get_user_dashboared_link( $user_link, 'dashboard' )) ?>">
              <i class="fa fa-cog"></i>
              <span class="title"><?php st_the_language( 'user_dashboard' ) ?></span>
            </a>
          </li>
          <li class="item <?php if($sc == 'profile') echo 'active' ?> ">
            <a href="<?php echo add_query_arg( 'id_user', $url_id_user ,TravelHelper::get_user_dashboared_link( $user_link, 'profile' )) ?>">
              <i class="fa fa-cog"></i>
              <span class="title"><?php st_the_language( 'user_profile' ) ?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="user-content col-md-9">
    	<div class="st-page-bar">
        <ul class="page-breadcrumb">
          <?php echo STUser_f::st_get_breadcrumb_partner() ?>
        </ul>
      </div>
        <?php
        	// print_r($sc);
        	// print_r (get_object_vars( $current_user ));
        	echo st()->load_template( 'user/customer/customer' , $sc , get_object_vars( $current_user ) );
        ?>
    </div>
  </div>
</div>