<?php

if ( taxonomy_exists('room_facilities') ){
    
    $terms = get_the_terms(get_the_ID(), 'room_facilities');

    foreach($terms as $term){              
?>  
        <li class="m-b-0">
            <span class="label label-default">                
        <?php 
            if (function_exists('get_tax_meta') and $icon = get_tax_meta($term->term_id, 'st_icon')): ?>
                <i class="<?php echo TravelHelper::handle_icon($icon) ?>"></i> 
      <?php endif; ?>
                <?php echo esc_html( $term->name ) ?>
            </span>
        </li>
<?php

    } 
}

?>